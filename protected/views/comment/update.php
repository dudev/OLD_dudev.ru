<?php
/* @var $this CommentController */
/* @var $model Comment */

$this->breadcrumbs=array(
	'Коммантарии'=>array('index'),
	'Изменение комментария',
);
?>

<h1>Изменение комментария</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>