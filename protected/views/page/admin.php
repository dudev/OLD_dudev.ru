<?php
/* @var $this PageController */
/* @var $model Page */

$this->breadcrumbs=array(
	'Управление страницами',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#page-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление страницами</h1>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?> |
<?php echo CHtml::link('Создать страницу',Yii::app()->createUrl('page/create')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'page-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'pager'=>[
        'header'=>'',
        'nextPageLabel'=>'&gt;',
        'prevPageLabel'=>'&lt;',
        'firstPageLabel'=>'&lt;&lt;',
        'lastPageLabel'=>'&gt;&gt;',
    ],
    'template'=>'{items}{pager}',
	'columns'=>array(
		[
            'name'=>'title',
            'filter'=>false,
        ],
		[
            'name'=>'status',
            'value'=>'Lookup::item("PageStatus",$data->status)',
            'filter'=>Lookup::items('PageStatus'),
        ],
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
