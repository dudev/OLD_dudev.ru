<?php
/* @var $this PageController */
/* @var $model Page */

$this->breadcrumbs=array(
	$model->title,
);
?>

<h1><?php echo $model->title; ?></h1>

<?php echo $model->content; ?>
