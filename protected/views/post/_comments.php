<?php foreach($comments as $data) { ?>
<section class="meta">
	<p><?php echo CHtml::encode($data->author); ?> | <?php echo CHtml::encode($data->create_time); ?></p>
</section>
<p class="comment"><?php echo $data->content; ?></p>
<?php } ?>