<?php
$this->setPageTitle($model->title);
Yii::app()->clientScript->registerMetaTag($this->shortContent($model->content, 150, '', true), 'description');
Yii::app()->clientScript->registerMetaTag($model->tags, 'keywords');
$this->breadcrumbs = [$model->title];

Yii::app()->clientScript->registerScriptFile('http://yandex.st/highlightjs/7.3/highlight.min.js');
Yii::app()->clientScript->registerCssFile('http://yandex.st/highlightjs/7.3/styles/googlecode.min.css');
Yii::app()->clientScript->registerScript('highlight', 'hljs.initHighlightingOnLoad();');
?>
<h1><?php echo $model->title; ?></h1>

<div class="post">
	<?php if($model->is_bigimg_post) { ?><img src="/images/<?php echo CHtml::encode($model->id); ?>.jpg" align="right" onerror="$(this).remove();"><?php } ?>
	<?php echo $model->content; ?>
    <p>&copy; <a href="/">Русанов Евгений</a></p>
	<br clear="both">
	<section class="meta">
		<p><?php echo CHtml::encode($model->public_time); ?> | <?php echo $model->commentCount . ' комментариев'; ?> | <i>Раздел: <a href="/blog/cat/<?php echo $model->category->id; ?>/"><?php echo $model->category->name; ?></a></i> | Теги: <?php echo Tag::getListTags($model->tags); ?></p>
	</section>
</div>
<?php if($this->beginCache('SimilarPost_'.$model->category_id.'_'.$model->id, array('duration'=>3600*24*7))) { ?>
    <?php $this->widget('SimilarPost', ['category_id'=>$model->category_id, 'current_id'=>$model->id]); ?>
<?php $this->endCache(); } ?>
<div id="comments">
	<h2>Комментарии</h2>
	<?php if($model->commentCount>=1): ?>
		<?php $this->renderPartial('_comments',array(
			'post'=>$model,
			'comments'=>$model->comments,
		)); ?>
    <?php endif; ?>
    <h3>Добавить комментарий</h3>
    <?php if(Yii::app()->user->hasFlash('commentSubmitted')): ?>
        <div class="flash-success">
            <?php echo Yii::app()->user->getFlash('commentSubmitted'); ?>
        </div>
    <?php endif; ?>
    <?php $this->renderPartial('/comment/_form_user',array(
        'model'=>$comment,
    )); ?>
</div>