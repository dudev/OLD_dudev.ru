<div class="post">
	<?php echo CHtml::link(CHtml::encode($data->title), array('view', 'id'=>$data->id, 'nick'=>$data->nick), ['class'=>'h1']); ?>
    <?php echo CHtml::link(CHtml::image('/images/'.CHtml::encode($data->id).'-min.jpg', '', ['align'=>'left', 'width'=>200]), array('view', 'id'=>$data->id, 'nick'=>$data->nick)); ?>
	<?php echo $this->shortContent($data->content); ?>
	<section class="meta">
		<p><?php echo CHtml::encode($data->public_time); ?> | <?php echo $data->commentCount . ' комментариев'; ?> | <i>Раздел: <a href="/blog/cat/<?php echo $data->category->id; ?>/"><?php echo $data->category->name; ?></a></i> | Теги: <?php echo Tag::getListTags($data->tags); ?></p>
	</section>
</div>