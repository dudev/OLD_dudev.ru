<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
	'Разделы'=>array('admin'),
	'Создание раздела',
);
?>

<h1>Создание раздела</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>