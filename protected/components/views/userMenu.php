<nav>
    <ul>
        <li><?php echo CHtml::link('Создать пост',array('post/create')); ?></li>
        <li><?php echo CHtml::link('Посты',array('post/admin')); ?></li>
        <li><?php echo CHtml::link('Комментарии',array('comment/index'))
            . ' (' . Comment::model()->pendingCommentCount . ')'
            ; ?></li>
        <li><?php echo CHtml::link('Разделы',array('category/index')); ?></li>
        <li><?php echo CHtml::link('Страницы',array('page/index')); ?></li>
        <li><?php echo CHtml::link('Выход',array('site/logout')); ?></li>
    </ul>
</nav>