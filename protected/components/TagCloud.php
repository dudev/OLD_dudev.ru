<?php
Yii::import('zii.widgets.CPortlet');

class TagCloud extends CPortlet
{
    public $title='Теги';
    public $min_frequency = 1;

    protected function renderContent()
    {
        $tags=Tag::model()->findTagWeights($this->min_frequency);

        foreach($tags as $tag=>$weight)
        {
            $link=CHtml::link(CHtml::encode($tag), array('post/index','tag'=>$tag));
            echo CHtml::tag('span', array(
                'class'=>'tagcloudsize-'.$weight,
            ), $link)."\n";
        }
    }
}