<?php
Yii::import('zii.widgets.CPortlet');

class SimilarPost extends CPortlet {
    public $title='Похожие посты';
    public $category_id;
    public $current_id;

    protected function renderContent()
    {
        echo '<div><ul>';
        $posts = Yii::app()->db->createCommand()
            ->select('id, nick, title')
            ->from('{{post}}')
            ->where
                (
                    'category_id=:category_id and id != :current_id and status = ' . Post::STATUS_PUBLISHED . ' and public_time < ' . time(),
                    [
                        'category_id'=>$this->category_id,
                        'current_id'=>$this->current_id
                    ]
                )
            ->limit(5)
            ->order('RAND()')
            ->queryAll();
        foreach($posts as $post)
        {
            $link=CHtml::link(CHtml::encode($post['title']), array('post/view','id'=>$post['id'], 'nick'=>$post['nick']));
            echo CHtml::tag('li', [], $link);
        }
        echo '</ul></div>';
    }
}