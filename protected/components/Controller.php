<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	private $_pageTitle;
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	
	public function getPageTitle($full = true)
	{
		return $this->_pageTitle ?: Yii::app()->name;
	}
	
	public function setPageTitle($value)
	{
		$this->_pageTitle = Yii::app()->name.' - '.$value;
	}
	
	public static function shortContent($content, $length = 255, $suffix = '...', $removeTN = false){
		$text=strip_tags(preg_replace('#(<(script|style)(.*)/(script|style)>)#iu', '', $content));
        if($removeTN)
            $text=str_replace(array("\t", "\r", "\n"), [' ', '', ''], $text);
		if(mb_strlen($text) > $length) {
			if($pos = mb_strpos($text, ' ', $length)) {
                $text = mb_substr($text, 0, $pos);
            }
            else {
                $suffix = '';
            }
			return $text.$suffix;
		}
		else
			return $text;
	}

    public static function translit ($str)
    {
        $translit_array = array (
            'а'=>'a',   'б'=>'b', 'в'=>'v', 'г'=>'g', 'д'=>'d', 'е'=>'ye', 'ё'=>'yo', 'ж'=>'zh', 'з'=>'z',
            'и'=>'i',   'й'=>'y', 'к'=>'k', 'л'=>'l', 'м'=>'m', 'н'=>'n',  'о'=>'o',  'п'=>'p',
            'р'=>'r',   'с'=>'s', 'т'=>'t', 'у'=>'u', 'ф'=>'f', 'х'=>'h',  'ц'=>'c',  'ч'=>'ch', 'ш'=>'sh',
            'щ'=>'sch', 'ъ'=>'',  'ы'=>'y', 'ь'=>'',  'э'=>'e', 'ю'=>'yu', 'я'=>'ya',
            'А'=>'A',   'Б'=>'B', 'В'=>'V', 'Г'=>'G', 'Д'=>'D', 'Е'=>'YE', 'Ё'=>'YO', 'Ж'=>'ZH', 'З'=>'Z',
            'И'=>'I',   'Й'=>'Y', 'К'=>'K', 'Л'=>'L', 'М'=>'M', 'Н'=>'N',  'О'=>'O',  'П'=>'P',
            'Р'=>'R',   'С'=>'S', 'Т'=>'T', 'У'=>'U', 'Ф'=>'F', 'Х'=>'H',  'Ц'=>'C',  'Ч'=>'CH', 'Ш'=>'SH',
            'Щ'=>'SCH', 'Ъ'=>'',  'Ы'=>'Y', 'Ь'=>'',  'Э'=>'E', 'Ю'=>'YU', 'Я'=>'YA', ' '=>'-'
        );
        return preg_replace ('%[^\w-]+%iu', '', preg_replace ('%[-]{2,}%', '-', strtr ( trim( $str ), $translit_array ) ) );
    }

    private $_ifModifiedSince;
    private $_isSendLastModified = false;

    /**
     * Проверяет пришёл ли IF_MODIFIED_SINCE
     * Возвращает либо ложь, если не пришёл, либо пришедшее для проверки время
     * @return bool|int
     */
    protected function issetIfModifiedSince() {
        $this->_ifModifiedSince = false;

        if (isset($_ENV['HTTP_IF_MODIFIED_SINCE']))
            $this->_ifModifiedSince = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5));

        if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))
            $this->_ifModifiedSince = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));

        return $this->_ifModifiedSince;
    }

    /**
     * Отправляет хидер last_modified
     * @param $time int время последнего изменения в формате UNIX
     */
    protected function lastModified($time){
        $this->_isSendLastModified = true;

        !headers_sent() and header('Last-Modified: '. gmdate("D, d M Y H:i:s \G\M\T", $time));
    }

    /**
     * Проверяет изменился ли документ с даты в IF_MODIFIED_SINCE
     * Если нет, то сообщает, что документ не изменился
     * @param $time время последнего изменения в формате UNIX
     */
    protected function ifModifiedSince($time) {
        if($this->_ifModifiedSince >= $time){
            $this->sendHeaderNotModified();
        }
    }

    /**
     * Отправляет код Документ не изменился
     */
    protected function sendHeaderNotModified(){
        header('HTTP/1.1 304 Not Modified');
        Yii::app()->end();
    }

    protected function afterAction($action) {
        if(!$this->_isSendLastModified)
            $this->lastModified(time());

        parent::afterAction($action);
    }
}