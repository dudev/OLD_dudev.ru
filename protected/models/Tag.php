<?php

/**
 * This is the model class for table "{{tag}}".
 *
 * The followings are the available columns in table '{{tag}}':
 * @property integer $id
 * @property string $name
 * @property integer $frequency
 */
class Tag extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tag}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Тег',
			'frequency' => 'Частота',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		return;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tag the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function string2array($tags)
	{
	    return preg_split('/\s*,\s*/',trim($tags),-1,PREG_SPLIT_NO_EMPTY);
	}

	public static function array2string($tags)
	{
	    return implode(', ',$tags);
	}

	public function findTagWeights($min_frequency = 1) {
		$maxFrequency = Yii::app()->db->createCommand()
		    ->select('max(frequency)')
		    ->from('{{tag}}')
		    ->queryScalar();

		$tags=[];
		foreach($this->findAll('frequency >= :min_frequency', ['min_frequency' => $min_frequency]) as $tag) {
			$tags[ $tag['name'] ] = intval($tag['frequency'] / $maxFrequency * 7);
		}
		return $tags;
	}

    private static $_items=array();

    public static function items()
    {
        if(count(self::$_items)==0)
            self::loadItems();
        return self::$_items;
    }

    private static function loadItems()
    {
        self::$_items = [];
        $models = self::model()->findAll();
        foreach($models as $model)
            self::$_items[$model->name]=$model->name.' ('.$model->frequency.')';
    }

    public static function getListTags($tags) {
        $listTags=[];
        foreach (self::string2array($tags) as $tag) {
            $listTags[]= CHtml::link(CHtml::encode($tag), array('post/index', 'tag'=>$tag));
        }
        return implode(', ', $listTags);
    }

    public function updateFrequency($oldTags, $newTags) {
        $oldTags = self::string2array($oldTags);
        $newTags = self::string2array($newTags);
        $deletedTags = array_diff($oldTags, $newTags);
        $insertedTags = array_diff($newTags, $oldTags);

        if(count($deletedTags) > 0){
            $command=Yii::app()->db->createCommand('UPDATE {{tag}} SET frequency = frequency - 1 WHERE name IN ('.substr(str_repeat ('?,', count($deletedTags)), 0, -1).')');
            $command->execute(array_values($deletedTags));

            Yii::app()->db->createCommand('DELETE FROM {{tag}} WHERE frequency < 1')->execute();
        }

        if(count($insertedTags) > 0) {
            foreach($insertedTags as $tag) {
                if(Yii::app()->db->createCommand()->from('{{tag}}')->where('name=:name', array(':name'=>$tag))->queryRow()) {
                    $command=Yii::app()->db->createCommand('UPDATE {{tag}} SET frequency = frequency + 1 WHERE name = ?;');
                    $command->execute([$tag]);
                }
                else {
                    Yii::app()->db->createCommand()->insert('{{tag}}', ['name'=>$tag]);
                }
            }

        }
    }
}
