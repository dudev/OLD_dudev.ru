<?php

class PostController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	public function actions()
    {
        return array(
            'imageUpload'=>array(
                'class'=>'ext.redactor.actions.ImageUpload',
                'uploadPath'=>Yii::app()->basePath . '/../images/user_img',
                'uploadUrl'=>'/images/user_img',
                'uploadCreate'=>true,
                'permissions'=>0755,
            ),
            'fileUpload'=>array(
                'class'=>'ext.redactor.actions.FileUpload',
                'uploadPath'=>Yii::app()->basePath . '/../files/user_file',
                'uploadUrl'=>'/files/user_file',
                'uploadCreate'=>true,
                'permissions'=>0755,
            ),
            'imageList'=>array(
                'class'=>'ext.redactor.actions.ImageList',
                'uploadPath'=>Yii::app()->basePath . '/../images/user_img',
                'uploadUrl'=>'/images/user_img',
            ),
        );
    }

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
	        array('allow',  // �������� ���� ������������� ��������� �������� 'list' � 'show'
	            'actions'=>array('index', 'view', 'category'),
	            'users'=>array('*'),
	        ),
	        array('allow', // �������� ������������������� ������������� ��������� ����� ��������
	            'users'=>array('@'),
	        ),
	        array('deny',  // ��������� �������� ��
	            'users'=>array('*'),
	        ),
	    );
	}

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     * @param $nick
     */
    public function actionView($id, $nick)
    {
        $post=$this->loadModel($id, $nick, 'public_time < ' . time(), true);
        $comment=$this->newComment($post);

        $lastModified = isset($post->comments[0]) ? max($post->update_time, strtotime($post->comments[0]->create_time)) : $post->update_time;
        if($this->issetIfModifiedSince()) {
            $this->ifModifiedSince($lastModified);
        }
        else {
            $this->lastModified($lastModified);
        }

        $this->render('view',array(
            'model'=>$post,
            'comment'=>$comment,
        ));
    }

	protected function newComment($post)
	{
		 $comment=new Comment;

	    if(isset($_POST['ajax']) && $_POST['ajax']==='comment-form')
	    {
	        echo CActiveForm::validate($comment);
	        Yii::app()->end();
	    }

	    if(isset($_POST['Comment']))
	    {
	        $comment->attributes=$_POST['Comment'];
	        if($post->addComment($comment))
	        {
	            if($comment->status==Comment::STATUS_PENDING)
	                Yii::app()->user->setFlash('commentSubmitted','Спасибо за Ваш комментарий. Он будет опубликован после проверки.');
	            $this->refresh();
	        }
	    }
	    return $comment;
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Post;
        $model->setScenario('create');
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Post']))
		{
			unset($_POST['Post']['image']);
			$model->attributes=$_POST['Post'];
			$model->image=CUploadedFile::getInstance($model,'image');
			if($model->save()) {
				if ($model->image){
					$file = Yii::app()->basePath . '/../images/'.$model->id.'.jpg';
					$model->image->saveAs($file);
					
					$ih = new CImageHandler(); //Инициализация
					Yii::app()->ih 
						->load($file)
						->thumb('200', false)
						->save(Yii::app()->basePath . '/../images/'.$model->id.'-min.jpg')
						->reload() //Снова загрузка оригинала картинки
						->thumb('400', false)
						->save($file);
				}
				$this->redirect(array('update','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $model->setScenario('update');
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Post']))
		{
			unset($_POST['Post']['image']);
			$model->attributes=$_POST['Post'];
			$model->image=CUploadedFile::getInstance($model,'image');
			if($model->save()) {
				if ($model->image){
                    $file = Yii::app()->basePath . '/../images/'.$model->id.'.jpg';
                    $model->image->saveAs($file);

                    $ih = new CImageHandler(); //Инициализация
                    Yii::app()->ih
                        ->load($file)
                        ->thumb('200', false)
                        ->save(Yii::app()->basePath . '/../images/'.$model->id.'-min.jpg')
                        ->reload() //Снова загрузка оригинала картинки
                        ->thumb('400', false)
                        ->save($file);
				}
				$this->refresh();
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
	    {
	        // we only allow deletion via POST request
	        $this->loadModel($id)->delete();

	        if(!isset($_GET['ajax']))
	            $this->redirect(array('index'));
	    }
	    else
	        throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$criteria=new CDbCriteria(array(
	        'condition'=>'status=' . Post::STATUS_PUBLISHED . ' and public_time < ' . time(),
	        'order'=>'public_time DESC',
	        'with'=>['commentCount', 'category'],
	    ));
	    if(isset($_GET['tag']))
	        $criteria->addSearchCondition('tags',$_GET['tag']);

	    $dataProvider=new CActiveDataProvider('Post', array(
	        'pagination'=>
	        	[
                    'pageSize'=>15,
                    'pageVar'=>'pg',
                ],
	        'criteria'=>$criteria,
	    ));

	    $this->render('index',array(
	        'dataProvider'=>$dataProvider,
	    ));
	}

    public function actionCategory($category_id)
    {
        $category = Category::item($category_id);
        if($category===null)
            throw new CHttpException(404,'Категория не существует.');

        $criteria=new CDbCriteria(array(
            'condition'=>'status = ' . Post::STATUS_PUBLISHED . ' and category_id = ' . $category_id . ' and public_time < ' . time(),
            'order'=>'public_time DESC',
            'with'=>['commentCount', 'category'],
        ));

        $dataProvider=new CActiveDataProvider('Post', array(
            'pagination'=>
                ['pageSize'=>15],
            'criteria'=>$criteria,
        ));

        $this->render('category',array(
            'dataProvider'=>$dataProvider,
            'categoryName'=>$category->name,
        ));
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Post('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Post']))
			$model->attributes=$_GET['Post'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Post the loaded model
	 * @throws CHttpException
	 */
	private $_model;

    public function loadModel($id, $nick = false, $condition = '', $is_redirect = false)
    {
        if($this->_model===null)
        {
            if(Yii::app()->user->isGuest) {
                if(!empty($condition)) $condition .= ' and ';
                $condition .= '(status='.Post::STATUS_PUBLISHED.' OR status='.Post::STATUS_ARCHIVED.')';
            }

            if($nick) {
                if($is_redirect) {
                    $this->_model=Post::model()->findByPk($id, $condition);
                    if($this->_model->nick != $nick)
                        $this->redirect(['post/view', 'id'=>$id, 'nick'=>$this->_model->nick], false, 301);
                }
                else {
                    if(!empty($condition)) $condition .= ' and ';
                    $condition.='nick=:nick';
                    $this->_model=Post::model()->findByPk($id, $condition, ['nick'=>$nick]);
                }
            }
            else {
                $this->_model=Post::model()->findByPk($id, $condition);
            }

            if($this->_model===null)
                throw new CHttpException(404,'Страница не найдена.');
        }
        return $this->_model;
    }

	/**
	 * Performs the AJAX validation.
	 * @param Post $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='post-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
